Title: Design Drawer

Screenshot:
![](https://user-images.githubusercontent.com/624713/73392601-5ca47c80-4297-11ea-86a8-e3cd2539145e.png)

Description: I originally made this in processing for another class. It should draw lines as well as rectangles
based on the mouse position. I was unabel to get the rectangles to work. Another difficulty I had was getting the
background to not draw every frame. In processing this does not happen so the lines would overlap nicely.